/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.clasebd2809.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.clasebd2809.dao.CandidatosJpaController;
import root.clasebd2809.dao.exceptions.NonexistentEntityException;
import root.clasebd2809.entity.Candidatos;

/**
 *
 * @author Ripley
 */
@WebServlet(name = "CandidatoController", urlPatterns = {"/CandidatoController"})
public class CandidatoController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CandidatoController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CandidatoController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CandidatosJpaController dao=new CandidatosJpaController();
        String accion=request.getParameter("accion");

        if (accion.equals("crear")){
        request.getRequestDispatcher("index.jsp").forward(request, response);
        } 
        
        if (accion.equals("eliminar")){
            try {
                String seleccion=request.getParameter("seleccion");
               
                dao.destroy(seleccion);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(CandidatoController.class.getName()).log(Level.SEVERE, null, ex);
            }
              List<Candidatos> listaCandidatos=dao.findCandidatosEntities();
         
        request.setAttribute("lista", listaCandidatos);
        
       request.getRequestDispatcher("listar.jsp").forward(request, response);
                
        }
        
        if (accion.equals("ver")){ 

            String seleccion=request.getParameter("seleccion");
            
             Candidatos can= dao.findCandidatos(seleccion);
             
            request.setAttribute("candidato", can);
            request.getRequestDispatcher("editar.jsp").forward(request, response);      
        }            
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String botonseleccionado =request.getParameter("accion");
        
        
         CandidatosJpaController dao=new CandidatosJpaController();
        try {
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String edad = request.getParameter("edad");
            String domicilio = request.getParameter("domicilio");
            
            
            Candidatos candidatos=new Candidatos();
            candidatos.setRut(rut);
            candidatos.setNombre(nombre);
            candidatos.setApellido(apellido);
            candidatos.setEdad(edad);
            candidatos.setDomicilio(domicilio);
            
            
            
           if (botonseleccionado.equals("grabar")){
           
            dao.create(candidatos);
           }
           else{
               
             dao.edit(candidatos);
               
           }
           
            
         
        } catch (Exception ex) {
            Logger.getLogger(CandidatoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        
        List<Candidatos> listaCandidatos=dao.findCandidatosEntities();
         
        request.setAttribute("lista", listaCandidatos);
        
       request.getRequestDispatcher("listar.jsp").forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
