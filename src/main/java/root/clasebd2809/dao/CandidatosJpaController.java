/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.clasebd2809.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.clasebd2809.dao.exceptions.NonexistentEntityException;
import root.clasebd2809.dao.exceptions.PreexistingEntityException;
import root.clasebd2809.entity.Candidatos;

/**
 *
 * @author ccruces
 */
public class CandidatosJpaController implements Serializable {

    public CandidatosJpaController() {
     
    }
    private EntityManagerFactory emf =  Persistence.createEntityManagerFactory("my_persistence_unit"); 


    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Candidatos candidatos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(candidatos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCandidatos(candidatos.getRut()) != null) {
                throw new PreexistingEntityException("Candidatos " + candidatos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Candidatos candidatos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            candidatos = em.merge(candidatos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = candidatos.getRut();
                if (findCandidatos(id) == null) {
                    throw new NonexistentEntityException("The candidatos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Candidatos candidatos;
            try {
                candidatos = em.getReference(Candidatos.class, id);
                candidatos.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The candidatos with id " + id + " no longer exists.", enfe);
            }
            em.remove(candidatos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Candidatos> findCandidatosEntities() {
        return findCandidatosEntities(true, -1, -1);
    }

    public List<Candidatos> findCandidatosEntities(int maxResults, int firstResult) {
        return findCandidatosEntities(false, maxResults, firstResult);
    }

    private List<Candidatos> findCandidatosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Candidatos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Candidatos findCandidatos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Candidatos.class, id);
        } finally {
            em.close();
        }
    }

    public int getCandidatosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Candidatos> rt = cq.from(Candidatos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
